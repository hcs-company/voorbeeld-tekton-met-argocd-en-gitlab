apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: build-python

spec:
  params:
  - name: component_name
    type: string
    description: The main name for this component
  - name: git_url
    type: string
    description: git URL of the component to build
  - name: git_revision
    type: string
    description: revision of the component to clone
  - name: deploy_git_url
    type: string
    description: git URL of the deployment repo
  - name: deploy_git_branch
    type: string
    description: revision of the deployment repo to push into
  - name: image_url
    type: string
    description: Registry url to push into
  - name: TLSVERIFY
    type: string
    description: Verify TLS on git and registry endpoints
    default: "true"
  - name: gitlab_project_id
    type: string
    description: The gitlab proejct id to use

  workspaces:
  - name: app-source
  - name: deploy-source
  - name: results

  tasks:
  - name: notify-gitlab-start
    taskRef:
      kind: Task
      name: gitlab-set-build-status
    params:
    - name: project_id
      value: $(params.gitlab_project_id)
    - name: commit
      value: $(params.git_revision)
    - name: state
      value: running
    - name: name
      value: $(context.pipeline.name)
    - name: description
      value: $(context.pipelineRun.name)
    workspaces:
    - name: results
      workspace: results

  - name: checkout-source
    taskRef:
      kind: ClusterTask
      name: git-clone
    params:
    - name: url
      value: $(params.git_url)
    - name: revision
      value: $(params.git_revision)
    - name: sslVerify
      value: $(params.TLSVERIFY)
    workspaces:
    - name: output
      workspace: app-source

  - name: checkout-deploy
    taskRef:
      kind: ClusterTask
      name: git-clone
    params:
    - name: url
      value: $(params.deploy_git_url)
    - name: revision
      value: develop
    - name: sslVerify
      value: $(params.TLSVERIFY)
    workspaces:
    - name: output
      workspace: deploy-source

  - name: build-s2i-python
    taskRef:
      kind: Task
      name: s2i-python-3-hcs
    params:
    - name: TLSVERIFY
      value: $(params.TLSVERIFY)
    - name: IMAGE
      value: $(params.image_url)
    workspaces:
    - name: source
      workspace: app-source
    runAfter:
    - checkout-source

  - name: run-pytest
    taskRef:
      kind: Task
      name: pytest
    params:
    - name: image
      value: $(params.image_url)@$(tasks.build-s2i-python.results.imagedigest)
    workspaces:
    - name: results
      workspace: results
    runAfter:
    - build-s2i-python

  - name: kustomize-set-image
    taskref:
      kind: Task
      name: kustomize-set-image
    params:
    - name: image_name
      value: $(params.component_name)
    - name: new_name
      value: $(params.image_url)
    - name: imagedigest
      value: $(tasks.build-s2i-python.results.imagedigest)
    - name: deploy_branch
      value: $(params.deploy_git_branch)
    workspaces:
    - name: source
      workspace: deploy-source
    - name: results
      workspace: results
    runAfter:
    - build-s2i-python
    - run-pytest
    - checkout-deploy

  finally:
  - name: notify-gitlab-failure
    taskRef:
      kind: Task
      name: gitlab-set-build-status
    params:
    - name: project_id
      value: $(params.gitlab_project_id)
    - name: commit
      value: $(params.git_revision)
    - name: state
      value: check
    - name: checks
      value: pytest kustomize
    - name: name
      value: $(context.pipeline.name)
    - name: description
      value: $(context.pipelineRun.name)
    workspaces:
    - name: results
      workspace: results
