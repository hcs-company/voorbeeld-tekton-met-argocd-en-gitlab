# GitOps Pipelines met Tekton en ArgoCD

Dit repository bevat de code van een aantal pipelines die samen het
bouwen, testen, en uitrollen van een microservice over meerdere omgevingen
mogelijk maken.

## Uitgangspunten

- Elk component heeft zijn eigen git repository
- Er is ook een apart "deploy" repository
- De default branch in elk repository is "develop"
- Van code repositories bouwen we automatisch de volgende versies
  - De "develop" branch
  - Elke `release/*` branch
  - Elk Pull-Request/Merge-Request richting de `develop` branch
- Een successvolle build van een code repository zal in het deploy repo de
  branch met dezelfde branchnaam updaten met het gebouwde image. Als deze
  branch nog niet bestaat wordt deze aangemaakt vanaf de "develop" branch.
- Een push naar een branch van het "deploy" repository triggert de "deploy"
  pipeline. Deze zorgt dat de betreffende branch wordt uitgerold in een
  bijbehorende namespace.



## Build Python Pipeline
De `build-python` doorloopt de voglende stappen:
1. Build status van huidige commit in Gitlab op `Running` zetten
2. Git Checkout van de applicatie source code naar de `app-source` workspace
3. S2I build vanaf de `app-source` workspace
4. Tests draaien
   1. `pytest` vanuit het zojuistg gebouwde image
   2. test-artifacts opslaan op `results` workspace
5. Git checkout van deploy repo naar `deploy-source` workspace
6. Deployment repo updaten
   1. In `deploy-source` workspace naar gewenste git branch schakelen
   2. Met `kustomize edit set image` imagedigest van zojuist gebopuwde image
      opslaan
   3. `git commit` & `git push`
7. Altijd: Gitlab Build status updaten
   1. Build status rapporteren
   2. Eventuele test artifacts uploaden als comment op de commit.


## Deploy pipeline

De `deploy` pipeline doorloopt de volgende stappen
1. Git Checkout van de goede branch in het deploy repo
2. Aanmaken namemspace voor de branch, met rechten voor ArgoCD
3. Aanmaken ArgoCD Application object voor de goede branch. Deze applicatie
  gebruikt bij voorkeur een overlay met de naam van de branch, anders de
  `default` overlay, en als laatste fallback de `base`.
4. Wachten tot ArgoCD de applicatie als `Healthy` beschouwd.

## Uitrollen

Het uitrollen van de pipelines is relatief eenvoudig, maar eerst moeten er
enkele bestanden worden aangepast:

- `kustomize/components/common/gitlab-registry-secret.yml`
  Hierin moet voor de gebruikte registry een geldig pull-secret worden gezet.
  Dit moet een token met minimaal READ rechten zijn.
- `kustomize/components/common/gitlab-registry-secret.yml`
  Deze wordt gebruikt voor de status rapporten met de GitLab API. Zet onder
  `[gitlab]` de url en het token goed (standaard Project-Access Token)
- `isitfriday-isitfriday-develop.apps-crc.testing`
  Hetzelfde als het eerdere registry secret, maar deze heeft READ en WRITE rechten nodig.
- ` isitfriday-isitfriday-develop.apps-crc.testing`
  Zet in deze de `tekton.dev/git-*` annotaties goed voor je gebruikte gitlab,
  en voeg een ssh-privatekey toe met lees-rechten op je code-repositories, en schrijf-rechten
  op het deploy repository. Voor de (optionele) `known_hosts` kun je het
  `ssh-keyscan` commando gebruiken.
- `pipelines/pipelines/tests/*`
  Deze bestanden gebruik je om straks de pipelines te testen voor je de
  webhooks inschakelt, zorg dat de URLS en branches wijzen naar iets echts. Zet ook je
  `gitlab_project_id` op de goede waarde.
- `pipelines/triggers/build-triggerbinding.yml`
  Zet in deze je repo en registry URLS goed.

Na het aanpassen van alle parameters is het tijd om de objecten aan te maken:
1. `oc create -f pipelines/infra`
   Deze maakt de benodigde secrets en configmaps aan, en, heel belangrijk, een ArgoCD.
   Zorg er wel voor dat eerst de `openshift-gitops-operator` al geïnstalleerd is.
2. `oc create -f pipelines/tasks`
3. `oc create -f pipelines/pipelines`

Voordat we de triggers gaan aanmaken is het handig om de pipelines te testen:
1. `oc create -f pipelines/pipelines/tests/build-python-test.yml`
2. Wacht totdat deze klaar is
3. `oc create -f pipelines/pipelines/tests/deploy-test.yml`
4. Wacht totdat deze klaar is
5. `oc create -f pipelines/pipelines/tests/delete-pr-test.yml`
6. Wacht totdat deze klaar is

Als alle tests succesvol waren kun je de triggers aanmaken:
1. `oc create -f pipelines/triggers`

Nu kun je gaan testen door pushed te doen naar de `develop` en `release/*`
branches van je code repositories, en door pull-requests aan te maken, te
mergen, en te sluiten.
